﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AliPurchase.Models;
using AliPurchase.Views;
using RestSharp;
using MahApps.Metro.Controls;

namespace AliPurchase
{
    /// <summary>
    /// Logique d'interaction pour AuthenticateWindow.xaml
    /// </summary>
    public partial class AuthenticateWindow : MetroWindow
    {
        public AuthenticateWindow()
        {
            InitializeComponent();
#if DEBUG
            tbx_email.Text = "supplier01@alizon.fr";
            pbx_pwd.Password = "12345678";
#endif
        }

        private void Btn_Signin_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<String, String> userCredentials = new Dictionary<string, string>();

            userCredentials.Add("email", tbx_email.Text);
            userCredentials.Add("password", pbx_pwd.Password);
            IRestResponse response = Api.Post("login", userCredentials);

            Console.WriteLine(response.Content);

            ShellWindow mainWindow = new ShellWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
